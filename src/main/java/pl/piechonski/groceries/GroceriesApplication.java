package pl.piechonski.groceries;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class GroceriesApplication {
    public static void main(String[] args) {
        SpringApplication.run(GroceriesApplication.class, args);
    }
}
