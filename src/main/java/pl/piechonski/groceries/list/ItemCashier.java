package pl.piechonski.groceries.list;

import org.springframework.stereotype.Service;

@Service
public class ItemCashier {
    private final ItemRepository itemRepository;
    private final ItemSorter itemSorter;

    public ItemCashier(ItemRepository itemRepository, ItemSorter itemSorter) {
        this.itemRepository = itemRepository;
        this.itemSorter = itemSorter;
    }

    public void buy(Item item) {
        item.setBought(true);
        itemRepository.save(item);

        itemSorter.promote(item.getBefore(), item.getOrder());
    }

    public void restore(Item item) {
        item.setBought(false);

        itemSorter.demote(item.getBefore(), item.getOrder());

        itemRepository.save(item);
    }
}
