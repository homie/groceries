package pl.piechonski.groceries.list;

import org.springframework.stereotype.Service;
import pl.piechonski.groceries.product.Product;
import pl.piechonski.groceries.product.ProductRepository;

import java.util.List;

@Service
class ItemEditor {
    private final ItemRepository itemRepository;
    private final ProductRepository productRepository;
    private final ItemSorter itemSorter;
    private final ItemFinder itemFinder;

    public ItemEditor(
        ItemRepository itemRepository,
        ProductRepository productRepository,
        ItemSorter itemSorter,
        ItemFinder itemFinder
    ) {
        this.itemRepository = itemRepository;
        this.productRepository = productRepository;
        this.itemSorter = itemSorter;
        this.itemFinder = itemFinder;
    }

    public void increase(Item item, Item edited) {
        item.addAmount(edited.getAmount());
        itemRepository.save(item);
    }

    public void edit(Item item, Item edited) {
        if (item.getBefore() == null && edited.getBefore() != null ||
            item.getBefore() != null && edited.getBefore() == null ||
            item.getBefore() != null &&
            edited.getBefore() != null &&
            item.getBefore().compareTo(edited.getBefore()) != 0) {

            itemSorter.promote(item.getBefore(), item.getOrder());

            Item last = itemFinder.findLast(edited.getBefore());
            item.setOrder(last != null ? last.getOrder() + 1 : 1);
        }

        item.setAmount(edited.getAmount());
        item.setUnit(edited.getUnit());
        item.setName(edited.getName());
        item.setAfter(edited.getAfter());
        item.setBefore(edited.getBefore());

        itemRepository.save(item);
    }

    public void delete(Item item) {
        List<Product> products = productRepository.findByName(item.getName());
        products.forEach(productRepository::delete);

        itemRepository.delete(item);
    }
}
