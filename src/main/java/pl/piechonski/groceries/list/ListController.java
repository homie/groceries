package pl.piechonski.groceries.list;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.piechonski.groceries.log.Log;
import pl.piechonski.groceries.log.Logger;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api")
class ListController {
    private final ItemFinder itemFinder;
    private final ItemRepository itemRepository;
    private final Logger logger;
    private final ItemService itemService;

    public ListController(
        ItemFinder itemFinder,
        ItemRepository itemRepository,
        Logger logger,
        ItemService itemService
    ) {
        this.itemFinder = itemFinder;
        this.itemRepository = itemRepository;
        this.logger = logger;
        this.itemService = itemService;
    }

    @GetMapping("/items")
    public List<Category> getItems() {
        return itemFinder.getCategories();
    }

    @PostMapping("/add")
    public ResponseEntity<String> add(@RequestBody Item item) {
        Item existingItem = itemRepository.findByBoughtAndNameAndUnit(false, item.getName(), item.getUnit());

        if (existingItem == null) {
            itemService.add(item);
            logger.log(Log.Action.CREATE, item);
        } else {
            itemService.increase(existingItem, item);
            logger.log(Log.Action.EDIT, item);
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping("/item/{id}/edit")
    public ResponseEntity<String> edit(@PathVariable("id") long id, @RequestBody Item edited) {
        Item item = itemRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(""));

        itemService.edit(item, edited);
        logger.log(Log.Action.EDIT, item);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/item/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") long id) {
        Item item = itemRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(""));

        itemService.delete(item);
        // TODO: log

        return ResponseEntity.ok().build();
    }

    @PostMapping("/item/{id}/move")
    public ResponseEntity<String> move(@PathVariable("id") long id, @RequestBody Move move) {
        Item item = itemRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(""));

        itemService.move(item, move);

        logger.log(Log.Action.MOVE, item);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/item/{id}/buy")
    public ResponseEntity<String> buy(@PathVariable("id") long id) {
        Item item = itemRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(""));

        itemService.buy(item);

        logger.log(Log.Action.BUY, item);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/item/{id}/restore")
    public ResponseEntity<String> restore(@PathVariable("id") long id) {
        Item item = itemRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(""));

        itemService.restore(item);
        logger.log(Log.Action.RESTORE, item);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/history")
    public List<Item> getHistory() {
        return itemRepository.findByBoughtOrderByLastModifiedDateDesc(true);
    }
}
