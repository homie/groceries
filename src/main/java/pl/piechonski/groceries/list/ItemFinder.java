package pl.piechonski.groceries.list;

import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
class ItemFinder {
    private final ItemRepository itemRepository;

    public ItemFinder(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Category> getCategories() {
        List<Item> items = itemRepository.findByBoughtOrderByOrder(false);
        Map<String, Category> categoriesMap = new HashMap<>();

        Date now = new Date();

        for (Item item : items) {
            String dayKey = createDayKey(item.getBefore());
            Category category = categoriesMap.getOrDefault(dayKey, null);

            if (category == null) {
                category = new Category(
                    item.getBefore() == null
                        ? "undefined"
                        : new SimpleDateFormat("EEEE (yyyy-MM-dd)").format(item.getBefore()),
                    item.getBefore(),
                    item.getBefore() != null && item.getBefore().before(now)
                );
                categoriesMap.put(dayKey, category);
            }

            category.getItems().add(item);
        }

        List<Category> categories = new ArrayList<>(categoriesMap.values());

        categories.sort((category1, category2) -> {
            if (category1.getDate() == null && category2.getDate() == null) {
                return 0;
            }

            if (category1.getDate() == null) {
                return now.compareTo(category2.getDate());
            }

            if (category2.getDate() == null) {
                return now.compareTo(category1.getDate());
            }

            return category1.getDate().compareTo(category2.getDate());
        });

        return categories;
    }

    private String createDayKey(Date date) {
        return date == null ? "undefined" : date.toString();
    }

    public List<Item> findBelow(Date date, Integer order) {
        return date == null
            ? itemRepository.findByBoughtAndBeforeIsNullAndOrderGreaterThanEqual(false, order)
            : itemRepository.findByBoughtAndBeforeIsAndOrderGreaterThanEqual(false, date, order);
    }

    public Item findLast(Date date) {
        return date == null
            ? itemRepository.findTopByBoughtAndBeforeIsNullOrderByOrderDesc(false)
            : itemRepository.findTopByBoughtAndBeforeIsOrderByOrderDesc(false, date);
    }
}
