package pl.piechonski.groceries.list;

import org.springframework.stereotype.Service;

@Service
class ItemService {
    private final ItemCreator itemCreator;
    private final ItemEditor itemEditor;
    private final ItemSorter itemSorter;
    private final ItemCashier itemCashier;

    ItemService(ItemCreator itemCreator, ItemEditor itemEditor, ItemSorter itemSorter, ItemCashier itemCashier) {
        this.itemCreator = itemCreator;
        this.itemEditor = itemEditor;
        this.itemSorter = itemSorter;
        this.itemCashier = itemCashier;
    }

    public void add(Item item) {
        itemCreator.add(item);
    }

    public void increase(Item item, Item edited) {
        itemEditor.increase(item, edited);
    }

    public void edit(Item item, Item edited) {
        itemEditor.edit(item, edited);
    }

    public void delete(Item item) {
        itemEditor.delete(item);
    }

    public void move(Item item, Move move) {
        itemSorter.move(item, move);
    }

    public void buy(Item item) {
        itemCashier.buy(item);
    }

    public void restore(Item item) {
        itemCashier.restore(item);
    }
}
