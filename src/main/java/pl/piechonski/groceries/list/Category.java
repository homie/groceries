package pl.piechonski.groceries.list;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class Category {
    private String label;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private boolean overdue;
    private List<Item> items;

    public Category(String label, Date date, boolean overdue) {
        this.label = label;
        this.date = date;
        this.overdue = overdue;
        this.items = new ArrayList<>();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Date getDate() {
        return date;
    }

    public boolean isOverdue() {
        return overdue;
    }
}
