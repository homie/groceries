package pl.piechonski.groceries.list;

import java.util.Date;

class Move {
    private Date date;
    private int order;

    public Date getDate() {
        return date;
    }

    public int getOrder() {
        return order;
    }
}
