package pl.piechonski.groceries.list;

import org.springframework.stereotype.Service;
import pl.piechonski.groceries.product.Product;
import pl.piechonski.groceries.product.ProductRepository;

@Service
class ItemCreator {
    private final ItemRepository itemRepository;
    private final ProductRepository productRepository;
    private final ItemFinder itemFinder;

    public ItemCreator(
        ItemRepository itemRepository,
        ProductRepository productRepository,
        ItemFinder itemFinder
    ) {
        this.itemRepository = itemRepository;
        this.productRepository = productRepository;
        this.itemFinder = itemFinder;
    }

    public void add(Item item) {
        Item last = itemFinder.findLast(item.getBefore());
        item.setOrder(last != null ? last.getOrder() + 1 : 1);

        if (!productRepository.existsByName(item.getName())) {
            productRepository.save(new Product(item.getName()));
        }

        itemRepository.save(item);
    }
}
