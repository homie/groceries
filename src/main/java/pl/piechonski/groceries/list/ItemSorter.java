package pl.piechonski.groceries.list;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
class ItemSorter {
    private final ItemRepository itemRepository;
    private final ItemFinder itemFinder;

    public ItemSorter(ItemRepository itemRepository, ItemFinder itemFinder) {
        this.itemRepository = itemRepository;
        this.itemFinder = itemFinder;
    }

    public void move(Item item, Move move) {
        promote(item.getBefore(), item.getOrder());
        demote(move.getDate(), move.getOrder());

        item.setBefore(move.getDate());
        item.setOrder(move.getOrder());
        itemRepository.save(item);
    }

    public void promote(Date date, Integer order) {
        reorder(date, order, -1);
    }

    public void demote(Date date, Integer order) {
        reorder(date, order, 1);
    }

    private void reorder(Date date, Integer order, int change) {
        List<Item> reordered = itemFinder.findBelow(date, order);
        reordered.forEach(item -> item.setOrder(item.getOrder() + change));
        itemRepository.saveAll(reordered);
    }
}
