package pl.piechonski.groceries.list;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

interface ItemRepository extends CrudRepository<Item, Long> {
    List<Item> findByBoughtOrderByOrder(boolean bought);
    List<Item> findByBoughtAndBeforeIsNullAndOrderGreaterThanEqual(boolean bought, Integer order);
    List<Item> findByBoughtAndBeforeIsAndOrderGreaterThanEqual(boolean bought, Date date, int order);
    Item findTopByBoughtAndBeforeIsNullOrderByOrderDesc(boolean bought);
    Item findTopByBoughtAndBeforeIsOrderByOrderDesc(boolean bought, Date date);
    List<Item> findByBoughtOrderByLastModifiedDateDesc(boolean bought);
    Item findByBoughtAndNameAndUnit(boolean bought, String name, String unit);
}
