package pl.piechonski.groceries.assistant;

import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
class DueParser {
    public Due parse(String query) {
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            String dayOfWeekText = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.forLanguageTag("pl"));
            Matcher matcher = Pattern.compile(String.format("(na )?%s", dayOfWeekText)).matcher(query);

            if (matcher.find()) {
                return new Due(
                    filterDueDate(query, matcher.group(0)),
                    Date.from(
                        LocalDate.now()
                            .with(TemporalAdjusters.next(dayOfWeek))
                            .atStartOfDay(ZoneId.systemDefault())
                            .toInstant()
                    )
                );
            }
        }

        return new Due(query);
    }

    private String filterDueDate(String query, String dueDate) {
        if (dueDate == null) {
            return query;
        }

        String filteredQuery = query.replace(dueDate, "");
        filteredQuery = filteredQuery.replaceAll("\\(\\s*\\)", "");
        filteredQuery = filteredQuery.trim();

        return filteredQuery;
    }
}
