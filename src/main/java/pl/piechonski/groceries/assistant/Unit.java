package pl.piechonski.groceries.assistant;

class Unit {
    public final String text;
    public final String canonical;
    public final Placement placement;
    public final Boolean escape;

    enum Placement {BEFORE, AFTER}

    public Unit(
        String text,
        String canonical,
        Placement placement,
        Boolean escape
    ) {
        this.text = text;
        this.canonical = canonical;
        this.placement = placement;
        this.escape = escape;
    }
}
