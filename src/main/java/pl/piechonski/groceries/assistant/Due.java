package pl.piechonski.groceries.assistant;

import java.util.Date;

record Due(String query, Date date) {
    public Due(String query) {
        this(query, null);
    }
}
