package pl.piechonski.groceries.assistant;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.piechonski.groceries.list.Item;

import javax.annotation.Nullable;
import java.util.List;

@RestController
@RequestMapping("/api")
class AssistantController {
    private final Assistant assistant;

    public AssistantController(Assistant assistant) {
        this.assistant = assistant;
    }

    @GetMapping("/assistant")
    public @Nullable List<Item> parseQuery(@RequestParam String query) {
        return assistant.parseQuery(query);
    }
}
