package pl.piechonski.groceries.assistant;

import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
class QuantityParser {
    private static final Unit[] UNITS = {
        new Unit("kg", "kg", Unit.Placement.AFTER, true),
        new Unit("dag", "dag", Unit.Placement.AFTER, true),
        new Unit("g", "g", Unit.Placement.AFTER, true),
        new Unit("ml", "ml", Unit.Placement.AFTER, true),
        new Unit("l", "l", Unit.Placement.AFTER, true),
        new Unit("szt.", "szt.", Unit.Placement.AFTER, true),
        new Unit("szt", "szt.", Unit.Placement.AFTER, true),
        new Unit("x", "szt.", Unit.Placement.AFTER, true),
        new Unit("x", "szt.", Unit.Placement.BEFORE, true),
        new Unit("^", "szt.", Unit.Placement.BEFORE, false),
    };

    public Quantity parse(String query) {
        for (Unit unit : UNITS) {
            String unitText = unit.escape
                ? Pattern.quote(unit.text)
                : unit.text;

            String expression = unit.placement == Unit.Placement.BEFORE
                ? String.format("(?!<\\w)(?<unit>%s) *(?<amount>[0-9,.]+)(?!\\w)", unitText)
                : String.format("(?!<\\w)(?<amount>[0-9,.]+) *(?<unit>%s)(?!\\w)", unitText);

            Matcher matcher = Pattern.compile(expression).matcher(query);

            if (matcher.find()) {
                return createResult(query, matcher.group("amount"), unit, matcher.group(0));
            }
        }

        return createResult(query);
    }

    private Quantity createResult(String query) {
        return new Quantity(query);
    }

    private Quantity createResult(String query, String amountText, Unit unit, String quantity) {
        Double amount = parseAmount(amountText);
        String unitName = getUnitName(unit);

        String filteredQuery = filterQuantity(query, quantity);

        if (filteredQuery.length() == 0) {
            return new Quantity(query);
        }

        return new Quantity(filteredQuery, amount, unitName);
    }

    private Double parseAmount(String amountText) {
        if (amountText == null) {
            return null;
        }

        amountText = amountText.replace(",", ".");

        return Double.parseDouble(amountText);
    }

    private String getUnitName(Unit unit) {
        if (unit == null) {
            return null;
        }

        return unit.canonical;
    }

    private String filterQuantity(String query, String quantity) {
        if (quantity == null) {
            return query;
        }

        String filteredQuery = query.replace(quantity, "");
        filteredQuery = filteredQuery.replaceAll("\\(\\s*\\)", "");
        filteredQuery = filteredQuery.trim();

        return filteredQuery;
    }
}
