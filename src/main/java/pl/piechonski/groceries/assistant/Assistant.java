package pl.piechonski.groceries.assistant;

import org.springframework.stereotype.Service;
import pl.piechonski.groceries.list.Item;
import pl.piechonski.groceries.product.Product;
import pl.piechonski.groceries.product.ProductRepository;

import java.util.ArrayList;
import java.util.List;

@Service
class Assistant {
    private final QuantityParser quantityParser;
    private final DueParser dueParser;
    private final ProductRepository productRepository;

    public Assistant(QuantityParser quantityParser, DueParser dueParser, ProductRepository productRepository) {
        this.quantityParser = quantityParser;
        this.dueParser = dueParser;
        this.productRepository = productRepository;
    }

    public List<Item> parseQuery(String query) {
        Quantity quantity = quantityParser.parse(query);
        Due due = dueParser.parse(quantity.query());
        String finalQuery = due.query();
        List<Product> products = productRepository.findByNameContains(finalQuery);
        List<Item> items = new ArrayList<>();

        Item item = new Item();
        item.setAmount(quantity.amount());
        item.setUnit(quantity.unit());
        item.setBefore(due.date());
        item.setName(finalQuery);

        items.add(item);

        for (Product product : products) {
            if (product.getName().equals(item.getName())) {
                continue;
            }

            Item suggestion = new Item();
            suggestion.setAmount(quantity.amount());
            suggestion.setUnit(quantity.unit());
            suggestion.setBefore(due.date());
            suggestion.setName(product.getName());
            items.add(suggestion);
        }

        return items;
    }
}
