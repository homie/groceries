package pl.piechonski.groceries.assistant;

record Quantity(
    String query,
    Double amount,
    String unit
) {
    public Quantity(String query) {
        this(query, null, null);
    }
}
