package pl.piechonski.groceries.log;

import org.springframework.stereotype.Service;
import pl.piechonski.groceries.list.Item;

@Service
public class Logger {
    private final LogRepository logRepository;

    public Logger(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    public void log(Log.Action action, Item item) {
        logRepository.save(new Log(action, item));
    }
}
