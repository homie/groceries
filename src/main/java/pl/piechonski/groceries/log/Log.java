package pl.piechonski.groceries.log;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.piechonski.groceries.list.Item;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "logs")
@EntityListeners(AuditingEntityListener.class)
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Enumerated(EnumType.STRING)
    private Action action;
    @ManyToOne()
    private Item item;

    public enum Action {
        CREATE, EDIT, MOVE, BUY, RESTORE
    }

    @CreatedDate
    private Date createdDate;

    public Log() {

    }

    public Log(Action action, Item item) {
        this.action = action;
        this.item = item;
    }

    public int getId() {
        return id;
    }

    public Action getAction() {
        return action;
    }

    public Item getItem() {
        return item;
    }

    public Date getCreatedDate() {
        return createdDate;
    }
}
