package pl.piechonski.groceries.log;

import org.springframework.data.repository.CrudRepository;

interface LogRepository extends CrudRepository<Log, Long> {
}
