package pl.piechonski.groceries.product;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findByNameContains(String name);
    List<Product> findByName(String name);
    boolean existsByName(String name);
}
