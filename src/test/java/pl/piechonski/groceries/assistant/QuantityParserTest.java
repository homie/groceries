package pl.piechonski.groceries.assistant;

import junit.framework.TestCase;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class QuantityParserTest extends TestCase {
    private static Object[][] testParse_Parameters() throws Throwable {
        return new Object[][] {
            {"100ml chleba", new Quantity("chleba", 100d, "ml")},
            {"banan", new Quantity("banan", null, null)},
            {"100 ciastek", new Quantity("ciastek", 100d, "szt.")},
            {"5x tymbark", new Quantity("tymbark", 5d, "szt.")},
            {"1.5 kg czegoś", new Quantity("czegoś", 1.5d, "kg")},
            {"1,1ml śmietany 18%", new Quantity("śmietany 18%", 1.1d, "ml")},
            {"tymbark (5 szt.)", new Quantity("tymbark", 5d, "szt.")},
            {"10 sztaplarek", new Quantity("sztaplarek", 10d, "szt.")}
        };
    }

    @Test
    @Parameters(method = "testParse_Parameters")
    public void testParse(String query, Quantity expectedResult) {
        QuantityParser quantityParser = new QuantityParser();
        Quantity actualResult = quantityParser.parse(query);
        assertEquals(expectedResult.amount(), actualResult.amount());
        assertEquals(expectedResult.unit(), actualResult.unit());
        assertEquals(expectedResult.query(), actualResult.query());
    }
}