package pl.piechonski.groceries.assistant;

import junit.framework.TestCase;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.Locale;

@RunWith(JUnitParamsRunner.class)
public class DueParserTest extends TestCase {
    private static Object[][] testParse_Parameters() throws Throwable {
        return new Object[][] {
            {"chleb na poniedziałek", new Due("chleb", new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.forLanguageTag("en")).parse("Mon Jul 13 00:00:00 CEST 2020"))},
            {"chleb na wtorek", new Due("chleb", new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.forLanguageTag("en")).parse("Tue Jul 07 00:00:00 CEST 2020"))},
            {"chleb na niedziela", new Due("chleb", new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.forLanguageTag("en")).parse("Sun Jul 12 00:00:00 CEST 2020"))},
        };
    }

    @Test
    @Parameters(method = "testParse_Parameters")
    public void testParse(String query, Due expected) {
        final DueParser dueParser = new DueParser();
        final Due due = dueParser.parse(query);

        assertEquals(expected.query(), due.query());
        assertEquals(expected.date().toString(), due.date().toString());
    }
}